var express = require('express');
var socket = require('socket.io');
var nunjucks = require('nunjucks');
// var cors = require('cors');

// App setup
var port = 4000;
var app = express();
var server = app.listen(port, function(){
    console.log('listening for requests on port ', port);
});

// Static files
// app.use(cors());
app.use(express.static('public'));
// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });

//Configure Nunjucks
nunjucks.configure('views', {
    autoescape: true,
    express: app
})
app.set('view engine', 'html');

//Render views
app.get('/', function (req, res) {
   res.render('index.html', {
       port: port
   });
});

// Socket setup & pass server
var io = socket(server);

io.origins((origin, callback) => {
    callback(null, true);
});

io.on('connection', (socket) => {

    console.log('made socket connection', socket.id);

    // Handle chat event
    socket.on('chat', function(data){
        // console.log(data);
        io.sockets.emit('chat', data);
    });

});
